(function ($) {

var app = {

	init: function() {

		app.mobileNavigation();
		app.filtering();
		
	},

	filtering: function() {

		// Tab filtering for region and expertise
		$('.tab-filter a:not(:first)').addClass('inactive');
		$('.tab-content').hide();
		$('.tab-content:first').show();

		$('.tab-filter a').click( function() {
			var tab = $(this).attr('id');
			var $tabLink = $('.tab-filter a');
			var $tabContainer = $('.tab-container');

			if($(this).hasClass('inactive')) {
				$tabLink.addClass('inactive');
				$(this).removeClass('inactive');
				$tabLink.removeClass('active');
				$(this).addClass('active');

				$('.tab-content').hide();
				$('.' +tab+'').fadeIn('slow');
				$tabContainer.show();
			} else {
				$(this).removeClass('active').addClass('inactive');
				$tabContainer.hide();
			}
    	})

		// Active state for filter navigation
		$('.filter-navigation a').click( function() {
			$(this).parents().find('a.active').removeClass('active');
			$(this).addClass('active');
		})

	},

}

$(document).ready(function() {

	app.init();


    $(document).on('scroll', function() {
        var scrollPos = $(window).scrollTop()
          , $stickyNav = $("#header-wrapper");
        scrollPos > $("#global-nav").height() ? $stickyNav.addClass("is-sticky") : $stickyNav.removeClass("is-sticky")
    });

    
  
});

})(jQuery);